package de.obscure.choptwicelight

import android.content.Context
import android.content.SharedPreferences
import android.hardware.Sensor
import android.hardware.SensorManager
import android.preference.PreferenceManager


object Preferences {

    val EXTRA_WAKELOCK_TIMEOUT      = "EXTRA_WAKELOCK_TIMEOUT"
    val EXTRA_SENSOR_TUNING         = "EXTRA_SENSOR_TUNING"
    val EXTRA_THRESHOLD_VALUE       = "EXTRA_THRESHOLD_VALUE"
    val EXTRA_TUNED_THRESHOLD_VALUE = "EXTRA_TUNED_THRESHOLD_VALUE"
    val EXTRA_SERVICE_ENABLED       = "EXTRA_SERVICE_ENABLED"
    val EXTRA_CHOP_CHOP_THRESHOLD   = "EXTRA_CHOP_CHOP_THRESHOLD"
    val EXTRA_CHOP_CHOP_COUNTER     = "EXTRA_CHOP_CHOP_COUNTER"

    private fun edit(context: Context, f: (SharedPreferences.Editor) -> Unit) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val edit = sharedPref.edit()
        f(edit)
        edit.apply()
    }

    fun wakeLockTimeout(context: Context): Int {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(EXTRA_WAKELOCK_TIMEOUT, MainActivity.WAKE_LOCK_MIN)
    }

    fun wakeLockTimeout(context: Context, wakelockTimeout: Int) {
        edit(context, {it.putInt(EXTRA_WAKELOCK_TIMEOUT, wakelockTimeout)})
    }

    fun defaultMaxSensorDistance(context: Context): Float {
        val manager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val accelerometer = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        return accelerometer.maximumRange
    }

    fun thresholdValue(context: Context): Float {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(EXTRA_THRESHOLD_VALUE, defaultMaxSensorDistance(context) / 2)
    }

    fun thresholdValue(context: Context, threshold: Float) {
        edit(context, {it.putFloat(EXTRA_THRESHOLD_VALUE, threshold)})
    }

    fun thresholdTuning(context: Context): Int {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(EXTRA_SENSOR_TUNING, MainActivity.SENSOR_TUNING_DEFAULT)
    }

    fun thresholdTuning(context: Context, tuning: Int) {
        edit(context, {it.putInt(EXTRA_SENSOR_TUNING, tuning)})
    }

    fun tunedThresholdValue(context: Context): Float {
        return PreferenceManager.getDefaultSharedPreferences(context).getFloat(EXTRA_TUNED_THRESHOLD_VALUE, thresholdValue(context))
    }

    fun tunedThresholdValue(context: Context, tunedThreshold: Float) {
        edit(context, {it.putFloat(EXTRA_TUNED_THRESHOLD_VALUE, tunedThreshold)})
    }

    fun counter(context: Context): Int {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(EXTRA_CHOP_CHOP_COUNTER, 0)
    }

    fun counter(context: Context, inc: Int) {
        edit(context, {it.putInt(EXTRA_CHOP_CHOP_COUNTER, counter(context) + inc)})
    }

    fun serviceEnabled(context: Context): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(EXTRA_SERVICE_ENABLED, true)
    }

    fun serviceEnabled(context: Context, enabled: Boolean) {
        edit(context, {it.putBoolean(EXTRA_SERVICE_ENABLED, enabled)})
    }

    fun wakeLockWarningDialog(context: Context): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(EXTRA_CHOP_CHOP_THRESHOLD, true)
    }

    fun wakeLockWarningDialog(context: Context, show: Boolean) {
        edit(context, {it.putBoolean(EXTRA_CHOP_CHOP_THRESHOLD, show)})
    }

    fun registerListener(context: Context, listener: SharedPreferences.OnSharedPreferenceChangeListener) {
        PreferenceManager.getDefaultSharedPreferences(context).registerOnSharedPreferenceChangeListener(listener)
    }

    fun unregisterListener(context: Context, listener: SharedPreferences.OnSharedPreferenceChangeListener ) {
        PreferenceManager.getDefaultSharedPreferences(context).unregisterOnSharedPreferenceChangeListener(listener)
    }

}