package de.obscure.choptwicelight

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.widget.RemoteViews
import android.content.Context
import android.content.Intent
import de.obscure.choptwicelight.MainActivity.Companion.stringFormat

class ChopTwiceWidgetProvider : AppWidgetProvider {

    constructor()

    var remoteView: RemoteViews? = null

    var tunedThreshold      = 0f
    var serviceEnabled      = true
    var wakeLockTimeout     = 0
    var isNewWidgetInstance = true

    override fun onEnabled(context: Context?) {
        super.onEnabled(context)
    }

    override fun onDisabled(context: Context?) {
        super.onDisabled(context)
    }

    override fun onUpdate(context: Context?, appWidgetManager: AppWidgetManager?, appWidgetIds: IntArray?) {
        if(context != null && appWidgetManager != null && appWidgetIds != null) {
            val applicationContext = context.applicationContext
            val views = createRemoteView(applicationContext)
            views.setOnClickPendingIntent(R.id.service_enabled, createPendingIntent(applicationContext))
            if(isNewWidgetInstance)
                initializeWidget(applicationContext)
            setTunedThreshold(views)
            setWakeLock(applicationContext, views)
            setServiceEnabled(applicationContext, views)
            for(widgetId: Int in appWidgetIds) {
                appWidgetManager.updateAppWidget(widgetId, views)
            }
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        if(intent != null && context != null){
            when(intent.action) {
                MainActivity.ACTION_WIDGET_UPDATE -> {
                    isNewWidgetInstance = false
                    tunedThreshold  = intent.getFloatExtra(Preferences.EXTRA_TUNED_THRESHOLD_VALUE, Preferences.thresholdValue(context))
                    serviceEnabled  = intent.getBooleanExtra(Preferences.EXTRA_SERVICE_ENABLED, true)
                    wakeLockTimeout = intent.getIntExtra(Preferences.EXTRA_WAKELOCK_TIMEOUT, 0)
                    updateWidget(context)
                }
            }
        }
    }

    override fun onRestored(context: Context?, oldWidgetIds: IntArray?, newWidgetIds: IntArray?) {
        super.onRestored(context, oldWidgetIds, newWidgetIds)
    }

    private fun initializeWidget(context: Context?) {
        if(context != null) {
            serviceEnabled  = Preferences.serviceEnabled(context)
            tunedThreshold  = Preferences.tunedThresholdValue(context)
            wakeLockTimeout = Preferences.wakeLockTimeout(context)
        }
    }

    private fun updateWidget(context: Context) {
        val appWidgetManager = AppWidgetManager.getInstance(context)
        val widgetIds        = appWidgetManager.getAppWidgetIds(ComponentName(context, ChopTwiceWidgetProvider::class.java))
        onUpdate(context, appWidgetManager, widgetIds)
    }

    private fun createRemoteView(context: Context): RemoteViews {
        if(remoteView == null) {
            remoteView = RemoteViews(context.packageName, R.layout.chop_app_widget)
            return remoteView!!
        } else {
            return remoteView!!
        }
    }

    private fun createPendingIntent(context: Context): PendingIntent {
        val intent = Intent(SettingsChangedReceiver.ACTION_SERVICE_ON_OFF_CHANGED)
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    fun setTunedThreshold(views: RemoteViews) {
        val text = tunedThreshold.stringFormat(2)
        views.setTextViewText(R.id.widget_tuned_threshold, text)
    }

    fun setServiceEnabled(context: Context, views: RemoteViews) {
        val text = if(serviceEnabled) context.getString(R.string.widget_service_on) else context.getString(R.string.widget_service_off)
        views.setTextViewText(R.id.service_enabled, text)
    }

    fun setWakeLock(context: Context, views: RemoteViews) {
        val text = if (wakeLockTimeout < 1) {
            context.getString(R.string.wakelock_off)
        } else if(wakeLockTimeout >= MainActivity.WAKE_LOCK_MAX) {
            context.getString(R.string.wakelock_on)
        } else {
            "$wakeLockTimeout/${MainActivity.WAKE_LOCK_MAX-1}"
        }
        views.setTextViewText(R.id.widget_cpu_lock, text)
    }


}