package de.obscure.choptwicelight


interface OnChopChop {

    fun changed(time: Long, x: Float, y: Float): Unit

}