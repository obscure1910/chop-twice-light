package de.obscure.choptwicelight

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment


class SensorCalibrationDialog(val onPositive: () -> Unit) : AppCompatDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.sensor_calibration_warning, null)
        builder.setView(view)
        builder.setPositiveButton(R.string.sensor_calibration_dialog_ok, { dialogInterface, i ->
            onPositive()
            dialogInterface.cancel()
        })
        builder.setNegativeButton(R.string.sensor_calibration_dialog_cancel, { dialogInterface, i ->
            dialogInterface.cancel()
        })
        return builder.create()
    }

}