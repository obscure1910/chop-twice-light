package de.obscure.choptwicelight

import android.app.Dialog
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import android.support.v7.widget.AppCompatCheckBox


class CpuLockWarningDialog : AppCompatDialogFragment {

    constructor()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.cpu_lock_warning, null)
        val checkbox = view.findViewById(R.id.checkboxSkip_cpu_lock) as AppCompatCheckBox
        checkbox.setOnCheckedChangeListener({ button, checked ->
            Preferences.wakeLockWarningDialog(context, !checked)
        })
        builder.setView(view)
        builder.setPositiveButton(R.string.wakelock_dialog_ok, { dialogInterface, i ->
            dialogInterface.cancel()
        })
        return builder.create()
    }
}