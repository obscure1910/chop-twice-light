package de.obscure.choptwicelight

interface CompatCamera {

    fun isLightOn(): Boolean
    fun toggleLight()
    fun disconnect()

}