package de.obscure.choptwicelight

import android.Manifest
import android.app.AlertDialog
import android.widget.SeekBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatSeekBar
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.SwitchCompat
import android.support.v7.widget.AppCompatButton
import android.os.Bundle
import android.content.*
import android.content.pm.PackageManager
import android.os.Vibrator
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View


class MainActivity : AppCompatActivity() {

    companion object {
        val debug = true

        val ACTION_WIDGET_UPDATE      = "de.obscure.choptwicelight.ACTION_WIDGET_UPDATE"
        val PERMISSION_REQUEST_CAMERA = 10000

        val THRESHOLD_TUNING_MAX     = 20
        val SENSOR_TUNING_DEFAULT = THRESHOLD_TUNING_MAX / 2

        val WAKE_LOCK_MIN         = 0   // 1 means always disabled
        val WAKE_LOCK_MAX         = 61  // 61 means always enabled

        fun Float.stringFormat(precision: Int) = String.format("%1.${precision}f", this)

    }
    //views
    private var mSeekBarSensorTuning: AppCompatSeekBar?      = null
    private var mTextViewSensorTuningLbl: AppCompatTextView? = null
    private var mTextViewWakeLock: AppCompatTextView?       = null
    private var mSeekBarWakeLock: AppCompatSeekBar?         = null
    private var mTextViewWakeLockLbl: AppCompatTextView?    = null
    private var mToggleServiceEnabled: SwitchCompat?        = null
    private var mButtonSensorCalibration: AppCompatButton?  = null
    private var mTextViewSensorRangeLbl: AppCompatTextView? = null
    private var mTextViewSensorRange: AppCompatTextView?    = null
    private var mTextViewCounterLbl: AppCompatTextView?     = null
    private var mTextViewCounter: AppCompatTextView?        = null
    //values
    private var mServiceEnabled                             = true
    private var mWakeLockTimeout                            = WAKE_LOCK_MIN
    private var mSensorTuning                               = SENSOR_TUNING_DEFAULT
    private var mThresholdValue                             = -1f
    private var mTunedThresholdValue                        = -1f
    private var mChopCounter                                = 0
    private var mSuddenlyAllowPermission                    = false


    val onSharedPreferenceChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        mServiceEnabled      = Preferences.serviceEnabled(applicationContext)
        mWakeLockTimeout     = Preferences.wakeLockTimeout(applicationContext)
        mSensorTuning        = Preferences.thresholdTuning(applicationContext)
        mThresholdValue      = Preferences.thresholdValue(applicationContext)
        mTunedThresholdValue = Preferences.tunedThresholdValue(applicationContext)
        mChopCounter         = Preferences.counter(applicationContext)
        mTextViewCounter?.text = mChopCounter.toString()
        enableViews(mServiceEnabled)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //init views
        mSeekBarSensorTuning              = findViewById(R.id.seekSensorTuning) as AppCompatSeekBar
        mTextViewSensorTuningLbl          = findViewById(R.id.textSensorTuningLabel) as AppCompatTextView
        mSeekBarWakeLock                  = findViewById(R.id.seekWakeLock) as AppCompatSeekBar
        mTextViewWakeLock                 = findViewById(R.id.textWakeLock) as AppCompatTextView
        mTextViewWakeLockLbl              = findViewById(R.id.textWakeLockLabel) as AppCompatTextView
        mToggleServiceEnabled             = findViewById(R.id.toggle) as SwitchCompat
        mButtonSensorCalibration          = findViewById(R.id.buttonSensorCalibration) as AppCompatButton
        mTextViewCounterLbl               = findViewById(R.id.textChopChopCountLabel) as AppCompatTextView
        mTextViewCounter                  = findViewById(R.id.textChopChopCount) as AppCompatTextView
        mTextViewSensorRange              = findViewById(R.id.textSensorRange) as AppCompatTextView
        mSeekBarWakeLock?.max             = WAKE_LOCK_MAX
        mTextViewWakeLock?.text           = wakeLockTimeoutText(WAKE_LOCK_MIN)
        mSeekBarSensorTuning?.max         = THRESHOLD_TUNING_MAX
        mToggleServiceEnabled?.isChecked  = Preferences.serviceEnabled(applicationContext)
        //register listener
        Preferences.registerListener(applicationContext, onSharedPreferenceChangeListener)
        mToggleServiceEnabled?.setOnClickListener({ view ->
            sendBroadcast(Intent(SettingsChangedReceiver.ACTION_SERVICE_ON_OFF_CHANGED))
        })
        mSeekBarWakeLock?.setOnSeekBarChangeListener(onWakeLockTimeoutChangeListener)
        mSeekBarSensorTuning?.setOnSeekBarChangeListener(onSensorTuningChangeListener)
        mButtonSensorCalibration?.setOnClickListener(onSensorCalibrationButtonClickListener)
    }

    override fun onResume() {
        super.onResume()
        mServiceEnabled                  = Preferences.serviceEnabled(applicationContext)
        mSensorTuning                    = Preferences.thresholdTuning(applicationContext)
        mWakeLockTimeout                 = Preferences.wakeLockTimeout(applicationContext)
        mThresholdValue                  = Preferences.thresholdValue(applicationContext)
        mTunedThresholdValue             = Preferences.tunedThresholdValue(applicationContext)
        mTextViewSensorRange?.text       = mTunedThresholdValue.stringFormat(2)
        mSeekBarSensorTuning?.progress   = mSensorTuning
        mSeekBarWakeLock?.progress       = mWakeLockTimeout

        mTextViewWakeLock?.text          = wakeLockTimeoutText(mWakeLockTimeout)
        mToggleServiceEnabled?.isChecked = mServiceEnabled
        mTextViewCounter?.text           = Preferences.counter(applicationContext).toString()

        enableViews(mServiceEnabled)
        val thisActivity = this

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(thisActivity, Manifest.permission.CAMERA)) {
                AlertDialog.Builder(thisActivity)
                    .setTitle(R.string.permission_camera_title)
                    .setMessage(R.string.permission_camera_text)
                    .setPositiveButton(android.R.string.ok,{ dialogInterface, i ->
                        ActivityCompat.requestPermissions(thisActivity,
                                arrayOf(Manifest.permission.CAMERA), PERMISSION_REQUEST_CAMERA)
                    }).setNegativeButton(android.R.string.cancel, null).show()
            } else {
                ActivityCompat.requestPermissions(thisActivity, arrayOf(Manifest.permission.CAMERA), PERMISSION_REQUEST_CAMERA)
            }
        } else if(!mSuddenlyAllowPermission && mToggleServiceEnabled?.isChecked ?: false) {
            sendBroadcast(Intent(SettingsChangedReceiver.ACTION_RESTART_SERVICE))
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == PERMISSION_REQUEST_CAMERA && grantResults.contains(0) && mToggleServiceEnabled?.isChecked ?: false) {
            mSuddenlyAllowPermission = true
            sendBroadcast(Intent(SettingsChangedReceiver.ACTION_RESTART_SERVICE))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Preferences.unregisterListener(applicationContext, onSharedPreferenceChangeListener)
    }

    fun enableViews(isEnabled: Boolean) {
        mTextViewSensorTuningLbl?.isEnabled  = isEnabled
        mSeekBarSensorTuning?.isEnabled      = isEnabled
        mTextViewWakeLockLbl?.isEnabled     = isEnabled
        mTextViewWakeLock?.isEnabled        = isEnabled
        mSeekBarWakeLock?.isEnabled         = isEnabled
        mButtonSensorCalibration?.isEnabled = isEnabled
        mTextViewCounter?.isEnabled         = isEnabled
        mTextViewCounterLbl?.isEnabled      = isEnabled
        mTextViewSensorRange?.isEnabled     = isEnabled
        mTextViewSensorRangeLbl?.isEnabled  = isEnabled
    }

    fun wakeLockTimeoutText(time: Int): String {
        if(time <= WAKE_LOCK_MIN){
            return getString(R.string.wakelock_off)
        } else if(time == WAKE_LOCK_MAX) {
            return getString(R.string.wakelock_on)
        } else {
            val unit = if(time == 1) {
                getString(R.string.wakelock_unit_singular)
            } else {
                getString(R.string.wakelock_unit_plural)
            }
            return "$time $unit"
        }
    }

    /**
     * listener
     */

    val onSensorTuningChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            mSensorTuning = progress
            mTunedThresholdValue = SettingsChangedReceiver.tunedThreshold(mThresholdValue, progress)
            mTextViewSensorRange?.text = mTunedThresholdValue.stringFormat(2)
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) { }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            val intent = Intent(SettingsChangedReceiver.ACTION_THRESHOLD_CHANGED)
            intent.putExtra(Preferences.EXTRA_SENSOR_TUNING, mSensorTuning)
            intent.putExtra(Preferences.EXTRA_THRESHOLD_VALUE, mThresholdValue)
            intent.putExtra(Preferences.EXTRA_TUNED_THRESHOLD_VALUE, mTunedThresholdValue)
            sendBroadcast(intent)
        }
    }

    val onWakeLockTimeoutChangeListener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            mWakeLockTimeout = progress
            mTextViewWakeLock?.text = wakeLockTimeoutText(progress)
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) { }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
            if(mWakeLockTimeout > 0) {
                if(Preferences.wakeLockWarningDialog(applicationContext)) {
                    val dialog = CpuLockWarningDialog()
                    dialog.show(supportFragmentManager, "cpu_lock_warning")
                }
            }
            val intent = Intent(SettingsChangedReceiver.ACTION_WAKELOCK_CHANGED)
            intent.putExtra(Preferences.EXTRA_WAKELOCK_TIMEOUT, mWakeLockTimeout)
            sendBroadcast(intent)
        }
    }

    val onSensorCalibrationButtonClickListener = View.OnClickListener { v ->
        val dialog = SensorCalibrationDialog {
            enableViews(false)
            sendBroadcast(Intent(SettingsChangedReceiver.ACTION_STOP_SERVICE))
            ChopTwiceCalibrator(applicationContext, { d ->
                enableViews(true)
                val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibrator.vibrate(VibratorSettings.calibrationDone, -1)
                mThresholdValue = d
                mTunedThresholdValue = SettingsChangedReceiver.tunedThreshold(mThresholdValue, mSensorTuning)
                mSensorTuning = SENSOR_TUNING_DEFAULT
                mSeekBarSensorTuning?.progress = SENSOR_TUNING_DEFAULT
                mTextViewSensorRange?.text = mTunedThresholdValue.stringFormat(2)
                val intent = Intent(SettingsChangedReceiver.ACTION_THRESHOLD_CHANGED)
                intent.putExtra(Preferences.EXTRA_THRESHOLD_VALUE, mThresholdValue)
                intent.putExtra(Preferences.EXTRA_SENSOR_TUNING, mSensorTuning)
                intent.putExtra(Preferences.EXTRA_TUNED_THRESHOLD_VALUE, mTunedThresholdValue)
                sendBroadcast(intent)
            })
        }
        dialog.show(supportFragmentManager, "sensor_calibration")
    }

}
