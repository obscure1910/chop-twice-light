package de.obscure.choptwicelight

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat


class SettingsChangedReceiver : BroadcastReceiver {

    constructor()

    companion object {
        val ACTION_STOP_SERVICE             = "de.obscure.choptwicelight.ACTION_STOP_SERVICE"
        val ACTION_START_SERVICE            = "de.obscure.choptwicelight.ACTION_START_SERVICE"
        val ACTION_RESTART_SERVICE          = "de.obscure.choptwicelight.ACTION_RESTART_SERVICE"
        val ACTION_THRESHOLD_CHANGED        = "de.obscure.choptwicelight.ACTION_THRESHOLD_CHANGED"
        val ACTION_SERVICE_ON_OFF_CHANGED   = "de.obscure.choptwicelight.ACTION_SERVICE_ON_OFF_CHANGED"
        val ACTION_WAKELOCK_CHANGED         = "de.obscure.choptwicelight.ACTION_WAKELOCK_CHANGED"

        fun tunedThreshold(threshold: Float, tuning: Int): Float {
            val deviation = 50
            val default = MainActivity.SENSOR_TUNING_DEFAULT
            val normalizedTuning = tuning - default
            val result = threshold + (threshold * normalizedTuning * deviation / default / 100)
            return when(normalizedTuning) {
                0    -> threshold
                else -> result
            }
        }

    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if(context != null && intent != null){
            val applicationContext = context.applicationContext
            when(intent.action) {
                Intent.ACTION_BOOT_COMPLETED, Intent.ACTION_MY_PACKAGE_REPLACED, ACTION_START_SERVICE -> start(applicationContext)
                ACTION_STOP_SERVICE                                -> stop(applicationContext)
                ACTION_RESTART_SERVICE                             -> restart(applicationContext)
                ACTION_THRESHOLD_CHANGED                           -> updateThreshold(applicationContext, intent)
                ACTION_SERVICE_ON_OFF_CHANGED                      -> updateServiceEnabled(applicationContext)
                ACTION_WAKELOCK_CHANGED                            -> updateWakeLock(applicationContext, intent)
            }
        }
    }

    fun updateServiceEnabled(context: Context) {
        val serviceEnabled = !Preferences.serviceEnabled(context)
        Preferences.serviceEnabled(context, serviceEnabled)
        if(serviceEnabled)
            start(context)
        else {
            stop(context)
        }
        updateWidget(context, Preferences.tunedThresholdValue(context), Preferences.wakeLockTimeout(context), serviceEnabled)
    }

    fun updateWakeLock(context: Context, intent: Intent) {
        val wakeLock = intent.getIntExtra(Preferences.EXTRA_WAKELOCK_TIMEOUT, MainActivity.WAKE_LOCK_MIN)
        Preferences.wakeLockTimeout(context, wakeLock)
        restart(context)
        updateWidget(context, Preferences.tunedThresholdValue(context), wakeLock, Preferences.serviceEnabled(context))
    }

    fun updateThreshold(context: Context, intent: Intent) {
        val threshold = intent.getFloatExtra(Preferences.EXTRA_THRESHOLD_VALUE, Preferences.thresholdValue(context))
        val tuning =  intent.getIntExtra(Preferences.EXTRA_SENSOR_TUNING, MainActivity.SENSOR_TUNING_DEFAULT)
        val tunedThresholdValue = intent.getFloatExtra(Preferences.EXTRA_TUNED_THRESHOLD_VALUE, threshold)
        Preferences.thresholdValue(context, threshold)
        Preferences.thresholdTuning(context, tuning)
        Preferences.tunedThresholdValue(context, tunedThresholdValue)
        restart(context)
        updateWidget(context, tunedThresholdValue, Preferences.wakeLockTimeout(context), Preferences.serviceEnabled(context))
    }

    fun updateWidget(context: Context, tunedThresholdValue: Float, wakeLock: Int, serviceEnabled: Boolean) {
        val updateWidgetIntent = Intent(MainActivity.ACTION_WIDGET_UPDATE)
        updateWidgetIntent.putExtra(Preferences.EXTRA_TUNED_THRESHOLD_VALUE, tunedThresholdValue)
        updateWidgetIntent.putExtra(Preferences.EXTRA_SERVICE_ENABLED, serviceEnabled)
        updateWidgetIntent.putExtra(Preferences.EXTRA_WAKELOCK_TIMEOUT, wakeLock)
        context.sendBroadcast(updateWidgetIntent)
    }

    fun start(context: Context) {
        val isEnabled = Preferences.serviceEnabled(context)
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && isEnabled) {
            context.startService(ChopTwiceFlashLightService.createIntent(context.applicationContext))
        }
    }

    fun stop(context: Context) {
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            context.stopService(ChopTwiceFlashLightService.createIntent(context.applicationContext))
        }
    }

    fun restart(context: Context) {
        stop(context)
        start(context)
    }

}