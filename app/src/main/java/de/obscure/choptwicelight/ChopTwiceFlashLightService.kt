package de.obscure.choptwicelight

import android.app.Service
import android.content.*
import android.os.*
import android.content.Intent.ACTION_SCREEN_OFF
import android.content.Intent.ACTION_SCREEN_ON
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager


class ChopTwiceFlashLightService : Service(), SensorEventListener {

    companion object {
        val ACTION_FLASHLIGHT_ON     = "de.obscure.choptwicelight.FLASHLIGHT_ON"
        val ACTION_FLASHLIGHT_OFF    = "de.obscure.choptwicelight.FLASHLIGHT_OFF"

        val SAMPLING_INTERVAL = 80L

        fun createIntent(context: Context): Intent {
            val intent = Intent(context, ChopTwiceFlashLightService::class.java)
            return intent
        }
        fun ignoreException(f: () -> Any?) = try {f()} catch (ex: Exception){}
    }

    //hardware
    private var mSensorManager: SensorManager? = null
    private var mAccelerometer: Sensor?        = null
    private var mCamera:CompatCamera?          = null

    private var mWakeLock: PowerManager.WakeLock? = null
    private var mWakeLockTimeout                  = MainActivity.WAKE_LOCK_MIN

    //acceleration values
    private var mLastSample                    = 0L
    private var mChopChopDetector: OnChopChop? = null


    val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if(intent != null) {
                when(intent.action) {
                    ACTION_SCREEN_OFF     -> {
                        releaseWakeLock()
                        setWakeLock()
                    }
                    ACTION_SCREEN_ON      -> {
                        releaseWakeLock()
                    }
                    ACTION_FLASHLIGHT_OFF -> {
                        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                        vibrator.vibrate(VibratorSettings.offPattern, -1)
                        releaseWakeLock()
                        setWakeLock()
                    }
                    ACTION_FLASHLIGHT_ON  -> {
                        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                        vibrator.vibrate(VibratorSettings.onPattern, -1)
                        releaseWakeLock()
                        setWakeLock(permanent = true)
                    }
                }
            }
        }
    }

    fun setWakeLock(permanent: Boolean = false) {
        if(!(mWakeLock?.isHeld ?: false)){
            if(permanent) {
                mWakeLock?.acquire()
            } else {
                when(mWakeLockTimeout) {
                    in 1..MainActivity.WAKE_LOCK_MAX - 1 -> mWakeLock?.acquire(mWakeLockTimeout.toLong() * 1000 * 60)
                    MainActivity.WAKE_LOCK_MAX           -> mWakeLock?.acquire()
                }
            }
        }
    }

    fun releaseWakeLock() {
        if(mWakeLock?.isHeld ?: false){
            mWakeLock?.release()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        ignoreException{releaseWakeLock()}
        ignoreException{unregisterReceiver(broadcastReceiver)}
        ignoreException{mCamera?.disconnect()}
        ignoreException{mSensorManager?.unregisterListener(this)}
    }

    override fun onCreate() {
        super.onCreate()
        mCamera = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            NewCamera(applicationContext)
        } else {
            OldCamera(applicationContext)
        }
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mAccelerometer = mSensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        val powerManager = getSystemService(POWER_SERVICE) as PowerManager
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "ChopTwiceServiceWakeLock")
        mWakeLockTimeout = Preferences.wakeLockTimeout(applicationContext)
        val filter = IntentFilter()
        filter.addAction(ACTION_FLASHLIGHT_ON)
        filter.addAction(ACTION_FLASHLIGHT_OFF)
        filter.addAction(Intent.ACTION_SCREEN_OFF)
        filter.addAction(Intent.ACTION_SCREEN_ON)
        registerReceiver(broadcastReceiver, filter)
        val tunedThresholdValue = Preferences.tunedThresholdValue(applicationContext)
        mChopChopDetector = ChopTwiceDetector(this, mCamera, tunedThresholdValue)
        mSensorManager?.registerListener(this , mAccelerometer, SensorManager.SENSOR_DELAY_UI)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onBind(p0: Intent?): IBinder? { return null }

    override fun onSensorChanged(event: SensorEvent?) {
        val now = System.currentTimeMillis()
        val diffTime = now - mLastSample
        if(event != null && diffTime > SAMPLING_INTERVAL) {
            mLastSample = now
            mChopChopDetector?.changed(now, event.values[0], event.values[1])
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}


}