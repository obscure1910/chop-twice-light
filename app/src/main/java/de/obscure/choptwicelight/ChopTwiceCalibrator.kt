package de.obscure.choptwicelight

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Handler
import android.os.Looper


class ChopTwiceCalibrator(ctx: Context, onQuit: (Float) -> Unit) : OnChopChop, SensorEventListener {

    private var mLastX: Float? = null
    private var mLastY: Float? = null
    private var mMaxDistance: Float = -1f
    private var mSensorManager: SensorManager? = null
    private var mAccelerometer: Sensor?
    private var mLastSample: Long = 0

    private val mHandler = Handler(Looper.getMainLooper())
    private val duration = 5000L

    val quitRunnable = Runnable {
        onQuit(mMaxDistance)
    }

    init {
        mSensorManager = ctx.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mAccelerometer = mSensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mMaxDistance = -1f//mAccelerometer?.maximumRange ?: -1f
        mSensorManager?.registerListener(this , mAccelerometer, SensorManager.SENSOR_DELAY_UI)
        mHandler.postDelayed(quitRunnable, duration)
    }

    override fun changed(time: Long, x: Float, y: Float) {
        val lastX = (mLastX ?: x).toDouble()
        val lastY = (mLastY ?: y).toDouble()

        val distance = ChopTwiceDetector.distance(x.toDouble(), y.toDouble(), lastX, lastY)

        if(distance > mMaxDistance) {
            mMaxDistance = distance.toFloat()
        }
        mLastX = lastX.toFloat()
        mLastY = lastY.toFloat()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {
        val now = System.currentTimeMillis()
        val diffTime = now - mLastSample
        if(event != null && diffTime > ChopTwiceFlashLightService.SAMPLING_INTERVAL) {
            mLastSample = now
            changed(now, event.values[0], event.values[1])
        }
    }
}