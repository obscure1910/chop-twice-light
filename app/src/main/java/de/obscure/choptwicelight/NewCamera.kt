package de.obscure.choptwicelight

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.os.Handler
import android.view.Surface
import android.util.Size
import android.util.Log
import android.support.v4.content.ContextCompat


@SuppressLint("NewApi")
class NewCamera(val ctx: Context) : CompatCamera {

    companion object {
        val TAG = ("de.obscure.choptwicelight." + NewCamera::class.java.simpleName)
    }

    private val mCameraId: String      = "0"
    private val mCameraManager         = ctx.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    private val mCameraCharacteristics = mCameraManager.getCameraCharacteristics(mCameraId)
    private val mHandler               = Handler()

    private var mBuilder: CaptureRequest.Builder? = null
    private var mSurfaceTexture: SurfaceTexture?  = null
    private var mSurface: Surface?                = null
    private var mSession: CameraCaptureSession?   = null
    private var mCameraDevice: CameraDevice?      = null
    private var mFlashMode: Int                   = CameraMetadata.FLASH_MODE_OFF

    private fun disconnectOnError(f: () -> Unit) {
        try{
            f()
        } catch (ex: Exception){
            disconnect()
        }
    }

    private fun broadcastCurrentFlashMode() {
        when(isLightOn()) {
            true  -> ctx.sendBroadcast(Intent(ChopTwiceFlashLightService.ACTION_FLASHLIGHT_ON))
            false -> {
                ctx.sendBroadcast(Intent(ChopTwiceFlashLightService.ACTION_FLASHLIGHT_OFF))
                disconnect()
            }
        }
    }

    private val flashModeRunnable = Runnable {
        flashMode()
    }

    private fun postFlashModeRunnable() {
        mHandler.post(flashModeRunnable)
    }

    private fun mSessionListener() = object:CameraCaptureSession.StateCallback() {

        override fun onConfigured(session: CameraCaptureSession) {
            if (session.device == mCameraDevice) {
                mSession = session
            } else {
                session.close()
            }
        }

        override fun onConfigureFailed(session: CameraCaptureSession) {}
    }

    override fun isLightOn(): Boolean = when(mFlashMode) {
        CameraMetadata.FLASH_MODE_TORCH -> true
        else                            -> false
    }


    override fun toggleLight() {
        postFlashModeRunnable()
    }

    override fun disconnect() {
        ChopTwiceFlashLightService.ignoreException{mHandler.removeCallbacks(flashModeRunnable)}
        ChopTwiceFlashLightService.ignoreException{mSession?.close()}
        ChopTwiceFlashLightService.ignoreException{mCameraDevice?.close()}
        mCameraDevice = null
        mSession = null
        ChopTwiceFlashLightService.ignoreException{mSurfaceTexture?.release()}
        ChopTwiceFlashLightService.ignoreException{mSurface?.release()}
        mSurface = null
        mSurfaceTexture = null
    }

    inner class MyCameraDeviceStateCallback : CameraDevice.StateCallback() {

        override fun onOpened(camera: CameraDevice?) {
            if(camera != null) {
                mCameraDevice = camera
                try {
                    mBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
                } catch (e: Exception) {
                     Log.e(TAG, "Error while open camera", e)
                }
                postFlashModeRunnable()
            }
        }

        override fun onDisconnected(camera: CameraDevice?) {
            disconnect()
        }

        override fun onError(camera: CameraDevice?, error: Int) {
            Log.e(TAG, "DeviceStateError with code $error")
        }
    }

    private fun flashMode() {
        if(mCameraDevice == null) {
            openCamera()
        } else if(mSession == null) {
            startSession()
        } else {
            val b = mBuilder
            val s = mSession
            if(b != null && s != null) {
                disconnectOnError {
                    val toggledFlashMode = when(isLightOn()) {
                        true  -> CameraMetadata.FLASH_MODE_OFF
                        false -> CameraMetadata.FLASH_MODE_TORCH
                    }
                    b.set(CaptureRequest.FLASH_MODE, toggledFlashMode)
                    s.setRepeatingRequest(b.build(), null, null)
                    mFlashMode = toggledFlashMode
                    broadcastCurrentFlashMode()
                }
            }
            return
        }
    }

    private fun getSmallestSize(cameraId: String): Size {
        val outputSizes = mCameraCharacteristics
                .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
                .getOutputSizes(SurfaceTexture::class.java)
        if (outputSizes == null || outputSizes.isEmpty()) {
            throw IllegalStateException("Camera " + cameraId + "doesn't support any outputSize.")
        }
        var chosen = outputSizes[0]
        outputSizes
            .asSequence()
            .filter { chosen.width >= it.width && chosen.height >= it.height }
            .forEach { chosen = it }
        return chosen
    }

    private fun openCamera() {
        if(ContextCompat.checkSelfPermission(ctx, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && mCameraDevice == null){
            disconnectOnError {
                mCameraManager.openCamera(mCameraId, MyCameraDeviceStateCallback(), null)
            }
        }
    }

    private fun startSession() {
        mSurfaceTexture = SurfaceTexture(0)
        val size = getSmallestSize(mCameraId)
        mSurfaceTexture!!.setDefaultBufferSize(size.width, size.height)
        mSurface = Surface(mSurfaceTexture)
        val list = listOf(mSurface)
        mBuilder!!.addTarget(mSurface)
        mCameraDevice?.createCaptureSession(list, mSessionListener(), null)
        postFlashModeRunnable()
    }

}