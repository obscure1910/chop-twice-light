package de.obscure.choptwicelight

import android.content.Context
import android.content.Intent
import android.hardware.*
import android.os.Handler

@SuppressWarnings( "deprecation" )
class OldCamera(val ctx: Context) : CompatCamera {

    val mHandler = Handler()

    var mCamera: Camera? = null //Camera.open()
    var mFlashMode       = Camera.Parameters.FLASH_MODE_OFF

    private fun broadcastCurrentFlashMode() {
        when(isLightOn()) {
            true  -> ctx.sendBroadcast(Intent(ChopTwiceFlashLightService.ACTION_FLASHLIGHT_ON))
            false -> {
                ctx.sendBroadcast(Intent(ChopTwiceFlashLightService.ACTION_FLASHLIGHT_OFF))
                disconnect()
            }
        }
    }

    private fun disconnectOnError(f: () -> Unit) {
        try{
            f()
        } catch (ex: Exception){
            disconnect()
        }
    }

    private val flashModeRunnable = Runnable {
        flashMode()
    }

    private fun postFlashModeRunnable() {
        mHandler.post(flashModeRunnable)
    }

    override fun isLightOn(): Boolean = when(mFlashMode) {
        Camera.Parameters.FLASH_MODE_TORCH -> true
        else                               -> false
    }

    override fun toggleLight() {
        postFlashModeRunnable()
    }

    override fun disconnect() {
        ChopTwiceFlashLightService.ignoreException{mHandler.removeCallbacks(flashModeRunnable)}
        ChopTwiceFlashLightService.ignoreException{mCamera?.release()}
        mCamera = null
    }

    private fun openCamera() {
        mCamera = Camera.open()
        postFlashModeRunnable()
    }

    private fun flashMode(){
        disconnectOnError {
            if(mCamera == null){
                openCamera()
            } else {
                val toggledFlashMode = when(isLightOn()){
                    true  -> Camera.Parameters.FLASH_MODE_OFF
                    false -> Camera.Parameters.FLASH_MODE_TORCH
                }
                val params = mCamera?.parameters
                params?.flashMode = toggledFlashMode
                mCamera?.parameters = params
                mCamera?.startPreview()
                mFlashMode = toggledFlashMode
                broadcastCurrentFlashMode()
            }
        }
    }
}