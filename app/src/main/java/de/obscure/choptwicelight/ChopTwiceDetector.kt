package de.obscure.choptwicelight

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.util.Log
import de.obscure.choptwicelight.SettingsChangedReceiver.Companion.ACTION_RESTART_SERVICE


class ChopTwiceDetector(val context: Context, val camera: CompatCamera?, val threshold: Float) : OnChopChop {

    companion object {
        val TAG = ("de.obscure.choptwicelight." + ChopTwiceDetector::class.java.simpleName)
        val CHOP_COUNT   = 3
        val CHOP_TIMEOUT = 500
        val CHOP_CHOP_TIMEOUT = 1000
        fun distance(x: Double, y: Double, lastX: Double, lastY: Double): Double = Math.sqrt(
            (x - lastX) * (x - lastX) +
            (y - lastY) * (y - lastY)
        )
    }

    private var mLastX: Float?           = null
    private var mLastY: Float?           = null
    private var mChopDirection: Boolean? = null
    private var mLastChop                = 0L
    private var mLastChopChop            = 0L
    private var mChopCount               = 0

    fun onChopChop() {
        try {
            if(PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)) {
                camera?.toggleLight()
                Preferences.counter(context, 1)
            }
        } catch(ex: Exception) {
            context.sendBroadcast(Intent(ACTION_RESTART_SERVICE))
        }
    }

    override fun changed(time: Long, x: Float, y: Float): Unit {
        val lastX = (mLastX ?: x).toDouble()
        val lastY = (mLastY ?: y).toDouble()
        val currentDirection = direction(x, lastX.toFloat())
        val distance = distance(x.toDouble(), y.toDouble(), lastX, lastY)

        val adjustedThreshold = adjustThreshold()

        val isCoolDown = (time - mLastChopChop) < CHOP_CHOP_TIMEOUT

        if((time - mLastChop) > CHOP_TIMEOUT){ reset() }

        if(MainActivity.debug) {
            Log.d(TAG, "distance: $distance \t threshold: $adjustedThreshold \t direction: $currentDirection \t lastDirection: $mChopDirection")
        }

        if(distance > adjustedThreshold && mChopDirection != currentDirection && !isCoolDown){
            mChopCount += 1
            mLastChop = time
            mChopDirection = currentDirection
            if(mChopCount >= CHOP_COUNT) {
                reset()
                mLastChopChop = time
                onChopChop()
            }
        }
        mLastX = x
        mLastY = y
    }

    fun direction(x: Float, lastX: Float): Boolean {
        val diffX = x - lastX
        return diffX >= 0
    }

    private fun adjustThreshold(): Float {
        if(mChopCount == 0) {
            return threshold
        } else {
            return threshold * 0.7f
        }
    }

    private fun reset() {
        mChopCount = 0
        mLastChop = 0
        mChopDirection = null
    }

}